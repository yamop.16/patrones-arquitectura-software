import '@polymer/polymer/polymer-element.js';
import '@polymer/paper-styles/paper-styles.js';

const $_documentContainer = document.createElement('template');
$_documentContainer.innerHTML = `<dom-module id="global">
  <template>
    <style>
      .card {
        margin: 24px;
        padding: 16px;
        color: #757575;
        border-radius: 5px;
        background-color: #fff;
        width: 80%;
        @apply --shadow-elevation-12dp;
      }
    
      .title-app {
        @apply --paper-font-common-base;
        @apply --paper-font-common-nowrap;
        font-size: 80px;
        font-weight: 500;
        line-height: 80px;
        color: var(--paper-red-900);
      }
      
      .subtitle-app {
        @apply --paper-font-common-base;
        @apply --paper-font-common-nowrap;
        font-size: 50px;
        font-weight: 400;
        line-height: 50px;
        color: var(--paper-red-900);
      }
      
      .horizontal {
        @apply --layout-horizontal;
      }
      
      .vertical {
        @apply --layout-vertical;
      }
      
      .center {
        @apply --layout-center-justified;
      }
      
      .start {
        @apply --layout-start-justified;
      }
      
      .end {
        @apply --layout-end-justified;
      }
      
      .justified {
        @apply --layout-justified;
      }
      
      .h-100 {
        height: 100%;
      }
      
      .child {
        @apply --layout-flex;
      }
    </style>
  </template>
</dom-module>`;

document.head.appendChild($_documentContainer.content);