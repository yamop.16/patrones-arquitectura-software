import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@polymer/paper-styles/paper-styles.js';
import '@polymer/paper-spinner/paper-spinner.js';
import '@vaadin/vaadin-dialog/vaadin-dialog.js';

/**
 * `she-loading`
 * Loading component to show before loaded view
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class SPCDialog extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
        
        .horizontal {
          @apply --layout-horizontal;
        }
        
        .center {
          @apply --layout-center-justified;
        }
        
        .vertical {
          @apply --layout-vertical;
        }
        
        .description {
          @apply --paper-font-common-base;
          @apply --paper-font-common-nowrap;
          font-size: 20px;
          font-weight: 400;
          line-height: 28px;
          color: var(--paper-grey-900);
        }
        
        .about {
          @apply --paper-font-common-base;
          @apply --paper-font-common-nowrap;
          font-size: 16px;
          font-weight: 400;
          line-height: 20px;
          color: var(--paper-grey-800);
        }
        
        .margin-10 {
          margin: 10px;
        }
        
        paper-spinner.multi {
          --paper-spinner-layer-1-color: var(--paper-red-900);
          --paper-spinner-layer-2-color: var(--paper-red-900);
          --paper-spinner-layer-3-color: var(--paper-red-900);
          --paper-spinner-layer-4-color: var(--paper-red-900);
        }
        
        .button {
          background: var(--paper-red-900);
          color: #fff;
        }
        
        .question-title {
          @apply --paper-font-headline;
          color: var(--paper-grey-900);
        }
        
        .start {
          @apply --layout-start-justified;
        }
        
        .end {
          @apply --layout-end-justified;
        }
        
        .capitalize {
          text-transform: capitalize;
        }
        
        .max-w {
          max-width: 600px;
        }
        
        .wrap {
          white-space: normal;
        }
      </style>
      
      <vaadin-dialog id="dialog" no-close-on-esc no-close-on-outside-click>
        <template>
          <template id="loading" is="dom-if" if="[[isLoader]]">
            <div class="horizontal center">
              <div class="vertical center">
                <div class="horizontal center">
                  <paper-spinner class="multi" active></paper-spinner>
                </div>
                <div class="horizontal center">
                  <span class="description margin-10">[[text]]</span>
                </div>
              </div>
            </div>
          </template>
          <template id="confirm" is="dom-if" if="[[isAlert]]">
            <div class="horizontal start">
              <span class="question-title">[[title]]</span>
            </div>
            <br>
            <div class="max-w">
              <span class="about wrap">[[text]]</span>
            </div>
            <br>
            <div class="horizontal end">
              <paper-button on-tap="close" class="button">Ok</paper-button>
            </div>
          </template>
        </template>
      </vaadin-dialog>
    `;
  }
  
  ready(){
    super.ready();
    this.close();
  }
  
  static get properties() {
    return {
      text: {
        type: String,
        value: 'loading ...'
      },
      title: {
        type: String,
        value: 'Alert'
      },
      type: {
        type: String,
        value: 'loader',
        observer: '_typeChange'
      },
      isLoader: {
        type: Boolean,
        value: true
      },
      isAlert: {
        type: Boolean,
        value: false
      }
    };
  }
  
  close(){
    this.$.dialog.opened = false;
  }
  
  open(){
    this.$.dialog.opened = true;
  }
  
  _typeChange(value){
    this._closeAll();
    switch(value){
      case 'loader':
        this.isLoader = true;
        break;
      case 'alert':
        this.isAlert = true;
        break;
    }
  }
  
  _closeAll(){
    this.isLoader = false;
    this.isConfirm = false;
    this.isAlert = false;
  }

}

window.customElements.define('spc-dialog', SPCDialog);
