/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

class SPCData extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
        }
      </style>
      <iron-ajax
            method="[[method]]"
            id="spcdata"
            url="[[url]]"
            content-type="[[content]]"
            on-response="_handleResponse"
            handle-as="json"
            body='[[body]]'>
        </iron-ajax>
    `;
  }
  
  static get properties(){
    return {
      url: {
        type: String,
        value: ''
      },
      content: {
        type: String,
        value: ''
      },
      method: {
        type: String,
        value: ''
      },
      body: {
        type: String,
        value: ''
      },
      omdbApi: {
        type: String,
        value: '97e082bd'
      },
      query: {
        type: String,
        value: ''
      }
    };
  }
  
  _performRequest(){
    this.$.spcdata.generateRequest();
  }
  
  _handleResponse(response){
    try {
      let data = response.detail.response;
      this.dispatchEvent(new CustomEvent('done', {detail: {data, query: this.query}}));
    } catch(err) {
      console.log('Error in request');
    }
  }
  
  getMovieInfo(title){
    this.method = 'GET';
    this.url = `http://www.omdbapi.com/?apikey=${this.omdbApi}&t=${title}&plot=full&r=json`;
    this.query = 'getMovieInfo';
    this._performRequest();
    //application/json
  }
}

window.customElements.define('spc-data', SPCData);