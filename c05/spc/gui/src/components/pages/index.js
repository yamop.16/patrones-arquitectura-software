/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';

import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-button/paper-button.js';
import '../common/spc-dialog.js';

class IndexView extends PolymerElement {
  static get template() {
    return html`
      <style include="global">
        :host {
          display: block;
          --paper-input-container-focus-color: var(--paper-red-900);
          --paper-input-container-input: {
            @apply --paper-font-headline;
            color: var(--paper-grey-50) !important;
          };
          --paper-input-container: {
            @apply --paper-font-headline;
          }
        }
        
        .main {
          background-image: url('https://i.imgur.com/3839Q7x.png');
          height: 100vh;
        }
        
        .button {
          background: var(--paper-red-900);
          color: #fff;
        }
        
        .button[disabled] {
          background: var(--paper-grey-100);
          color: var(--paper-grey-900);
        }
      </style>

      <div class="main">
        <div class="vertical center h-100">
          <div class="horizontal center">
            <span class="title-app">Netflix Inc.</span>
          </div>
          <div class="horizontal center">
            <paper-input label="Movie/Serie" value="{{movie}}"></paper-input>
          </div>
          <div class="horizontal center">
            <paper-input label="Twitter username" value="{{username}}"></paper-input>
          </div>
          <div class="horizontal center">
            <paper-button class="button" on-tap="_performAnalysis">perform analysis</paper-button>
          </div>
        </div>
      </div>
      
      <spc-dialog id="dialog"></spc-dialog>
    `;
  }
  
  static get properties(){
    return {
      movie: {
        type: String,
        value: '',
        notify: true
      },
      username: {
        type: String,
        value: '',
        notify: true
      }
    };
  }
  
  ready(){
    super.ready();
    this._welcome();
  }
  
  _welcome(){
    this.$.dialog.type = 'alert';
    this.$.dialog.title = 'Welcome';
    this.$.dialog.text = 'Welcome to the system, please introduce the title of some series or original movie of Netflix as well as him username (e.g. @usernamexample) in Twitter to perform an analysis of sentiments in the last comments its from him';
    this.$.dialog.open();
  }
  
  _performAnalysis(){
    if(this.username !== '' && this.movie !== ''){
      window.history.pushState({}, null, 'detail');
      window.dispatchEvent(new CustomEvent('location-changed'));
    } else {
      this._invalidRequest();
    }
  }
  
  _invalidRequest(){
    this.$.dialog.type = 'alert';
    this.$.dialog.title = 'Alert';
    this.$.dialog.text = 'A movie title and twitter username are required for perform analysis';
    this.$.dialog.open();
  }
}

window.customElements.define('index-view', IndexView);
