/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import { setPassiveTouchGestures, setRootPath } from '@polymer/polymer/lib/utils/settings.js';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout.js';
import '@polymer/app-layout/app-header-layout/app-header-layout.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-selector/iron-selector.js';
import './styles/global.js';

// Gesture events like tap and track generated from touch will not be
// preventable, allowing for better scrolling performance.
setPassiveTouchGestures(true);

// Set Polymer's root path to the same value we passed to our service worker
// in `index.html`.
setRootPath(MyAppGlobals.rootPath);

class SPCApp extends PolymerElement {
  static get template() {
    return html`
      <style include="global">
        :host {
          --app-primary-color: #4285f4;
          --app-secondary-color: var(--paper-grey-50);
          display: block;
        }
        
        footer {
          position:fixed;
          background-color: var(--paper-grey-700);
          bottom:0;
          width:100%;
        }
        
        .content {
          height: 100%;
          overflow: auto;
        }
        
        .pad-20 {
          padding: 10px;
        }
        
        .footer-title {
          @apply --paper-font-body2;
          color: var(--paper-grey-50);
        }
        
        .footer-desc {
          @apply --paper-font-caption;
          color: var(--paper-grey-200);
        }
        
        a:link, a:hover, a:visited, a:hover, a:active {
          text-decoration: none;
        }
        
      </style>

      <app-location route="{{route}}" url-space-regex="^[[rootPath]]">
      </app-location>

      <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
      </app-route>

      <app-drawer-layout fullbleed="" narrow="{{narrow}}">

        <!-- Main content -->
        <app-header-layout has-scrolling-region="">

          <div class="content">
            <iron-pages selected="[[page]]" attr-for-selected="name" role="main">
              <index-view name="index" username="{{username}}" movie="{{movie}}"></index-view>
              <detail-view name="detail" username="{{username}}" movie="{{movie}}"></detail-view>
            </iron-pages>
          </div>
          <footer>
            <div class="pad-20 horizontal">
              <div class="vertical child">
                <div class="horizontal start">
                  <span class="footer-title">Microservices Style</span>
                </div>
                <div class="horizontal start">
                  <span class="footer-desc">Copyright © University Autonomous of Zacatecas 2018</span>
                </div>
              </div>
              <div class="vertical child center">
                <div class="horizontal end">
                  <span class="footer-desc">Perla Velasco-Elizondo</span>
                </div>
                <div class="horizontal end">
                  <a href="http://ymartinez.info">
                    <span class="footer-desc">Y. Alexander Martínez Padilla</span>
                  </a>
                </div>
              </div>
            </div>
          </footer>
        </app-header-layout>
      </app-drawer-layout>
    `;
  }

  static get properties() {
    return {
      page: {
        type: String,
        reflectToAttribute: true,
        observer: '_pageChanged'
      },
      routeData: Object,
      subroute: Object,
      movie: {
        type: String,
        value: ''
      },
      username: {
        type: String,
        value: ''
      }
    };
  }

  static get observers() {
    return [
      '_routePageChanged(routeData.page)'
    ];
  }

  _routePageChanged(page) {
    this.page = page ? page : 'index';
  }

  _pageChanged(page) {
    switch (page) {
      case 'detail':
        import('./components/pages/detail.js');
        break;
      case 'index':
        import('./components/pages/index.js');
        break;
      default:
        import('./components/pages/index.js');
    }
  }
}

window.customElements.define('spc-app', SPCApp);
